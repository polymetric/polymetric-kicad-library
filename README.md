# Polymetric KiCad Library

This library includes footprints, symbols and "prefabs" that I've made 
for KiCad, and are semi-required for most of my project files.

Prefabs are just schematics I've made that are designed to be copied 
into another project and used there. For example, a prefab might exist 
for a certain type of filter, the crystal and decoupling 
capacitors for a microcontroller, or a voltage regulator set up for a 
particular application
